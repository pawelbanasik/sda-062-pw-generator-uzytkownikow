package com.pawelbanasik;

public class User {

	private String name;
	private String lastName;
	private UserAddress userAddress;
	private UserSex userSex;
	private String ccn;
	private String pesel;
	private String birth;

	public User() {
	}

	public User(String name, String lastName, UserAddress userAddress, UserSex userSex, String ccn, String pesel,
			String birth) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.userAddress = userAddress;
		this.userSex = userSex;
		this.ccn = ccn;
		this.pesel = pesel;
		this.birth = birth;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public UserAddress getUserAddress() {
		return userAddress;
	}

	public UserSex getUserSex() {
		return userSex;
	}

	public String getCcn() {
		return ccn;
	}

	public String getPesel() {
		return pesel;
	}

	public String getBirth() {
		return birth;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public void setUserSex(UserSex userSex) {
		this.userSex = userSex;
	}

	public void setCcn(String ccn) {
		this.ccn = ccn;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	@Override
	public String toString() {
		return "Imie: " + name + ", Nazwisko: " + lastName + ", Adres [" + userAddress + "]" + ", Płeć: " + userSex
				+ ", CCN: " + ccn + ", PESEL: " + pesel + ", Data Urodzenia: " + birth;
	}

}
