package com.pawelbanasik;

public class UserAddress {

	private String street;
	private String city;
	private String no;
	private String zipCode;

	public UserAddress(String street, String city, String no, String zipCode) {
		super();
		this.street = street;
		this.city = city;
		this.no = no;
		this.zipCode = zipCode;
	}

	public String getStreet() {
		return street;
	}

	public String getCity() {
		return city;
	}

	public String getNo() {
		return no;
	}

	public String getZipCode() {
		return zipCode;
	}

	@Override
	public String toString() {
		return "Ulica: " + street + ", Miasto: " + city + ", Numer: " + no + ", Kod pocztowy: " + zipCode;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

}
