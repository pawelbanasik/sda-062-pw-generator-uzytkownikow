package com.pawelbanasik;

import java.io.FileNotFoundException;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {

		UserGenerator ug = new UserGenerator();
		for(int i = 0; i < 5; i++)
			// drukuje wszystkie dane z wywolania obiektu na raz bo jest nadpisana metoda toString();
			System.out.println(ug.createRandomUser()); 
		
	}

}
