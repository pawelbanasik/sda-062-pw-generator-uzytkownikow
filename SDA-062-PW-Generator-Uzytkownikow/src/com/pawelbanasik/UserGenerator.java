package com.pawelbanasik;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {

	private final String path = "resources\\";
	private List<String> files = Arrays.asList("im_f.txt", "im_m.txt", "miasta.txt", "ulice.txt", "nazwiska.txt");
	private Map<String, List<String>> resources = new HashMap<>();

	// ten konstruktor wczytuje w pole mapa resources dane z plikow wtedy jak
	// stworzymy obiekt typu UserGenerator
	// uruchamia metode readFile();
	public UserGenerator() throws FileNotFoundException {

		readFile();

	}

	// metoda ta wpisuje do mapy jako klucz nazwe pliku a jako wartosc liste
	// tworzy liste odczytywana z pliku po kluczu i wrzuca do mapy
	private void readFile() throws FileNotFoundException {
		for (String filename : files) {
			Scanner sc = new Scanner(new File(path + filename));

			String[] array = filename.split("\\.");

			String key = array[0].toLowerCase();

			List<String> list = new ArrayList<>();

			String currentLine = "";

			if (!resources.containsKey(key)) {
				while (sc.hasNextLine()) {
					currentLine = sc.nextLine();
					list.add(currentLine);
				}
			}
			sc.close();
			resources.put(key, list);
		}

	}

	// metoda ta zwraca losowo płeć
	private UserSex getRandomSex() {

		Random random = new Random();

		int randomNumber = random.nextInt(2);

		if (randomNumber == 0) {
			return UserSex.SEX_FEMALE;
		} else {
			return UserSex.SEX_MALE;
		}
	}

	// metoda na losowa liczbe z przedzialu od do sprytnie zapisana
	// mozna losowac tylko losowe liczby z przedzialu od zera do granicy
	// on to pomimo ograniczenia rozwiazal
	private int getRandomNumberFromRange(int from, int to) {

		Random random = new Random();
		int randomNumber = random.nextInt(to - from + 1) + from;
		return randomNumber;
	}

	// metoda tworzy losowa date w postaci String'a uzywajac metody tworzacej
	// losowo liczbe od do przekazywane jako parametry
	private String createRandomUserBirthDate() {

		GregorianCalendar gc = new GregorianCalendar();

		// rok jako losowa liczba losowanie
		int randomYear = getRandomNumberFromRange(1900, 1999);
		// ustwaiam ten rok jako dany rok w naszym obiekcie kalandarza
		gc.set(GregorianCalendar.YEAR, randomYear);
		// losowanie dnia roku (nie miesiaca, tylko od razu roku od 1 do
		// maksymalnego dnia w danym roku)
		int randomDayOfYear = getRandomNumberFromRange(1, gc.getActualMaximum(GregorianCalendar.DAY_OF_YEAR));
		gc.set(GregorianCalendar.DAY_OF_YEAR, randomDayOfYear);
		// tu jest zamiana z liczb na String ze wzgledu na dodanie myslników
		String randomDate = gc.get(GregorianCalendar.DAY_OF_MONTH) + "-" + gc.get(GregorianCalendar.MONTH) + "-"
				+ gc.get(GregorianCalendar.YEAR);
		
		return randomDate;

	}

	// metoda pbiera wartosc po kluczu z danej listy (zwraca stringa czyli jakas
	// losowa linijke)
	// trudna metoda do skumania
	private String getRandomValueByKey(String key) {

		Random random = new Random();

		if (resources.containsKey(key)) {
			int randomLine = random.nextInt(resources.get(key).size());
			return resources.get(key).get(randomLine);
		} else {
			return null;
		}
	}

	// metoda tworzy obiekt typu Adres (losowy)
	private UserAddress createRandomUserAddress() {
		// zamiana w stringa poprzez dopisanie pustego znaku string
		String randomNumber = getRandomNumberFromRange(1, 100) + "";

		UserAddress ua = new UserAddress(getRandomValueByKey("ulice"), getRandomValueByKey("miasta"), randomNumber,
				"12-300");

		return ua;

	}

	public User createRandomUser() {
		// dzieki pustemu konstruktorowi moge tak zrobic ze bez parametrow
		// tworze tu obiekt
		User user = new User();

		String fileSex = "im_f";
		UserSex sex = UserSex.SEX_FEMALE;
		if (getRandomSex() == UserSex.SEX_MALE) {
			fileSex = "im_m";
			sex = UserSex.SEX_MALE;
		}
		user.setUserSex(sex);
		user.setName(getRandomValueByKey(fileSex));
		user.setLastName(getRandomValueByKey("nazwiska"));
		user.setUserAddress(createRandomUserAddress());
		user.setBirth(createRandomUserBirthDate());

		return user;
	}

}
